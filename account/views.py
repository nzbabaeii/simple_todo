from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from .forms import LoginForm, RegisterForm

def register(request):
    if request.method == 'GET':
        form = RegisterForm()
        return render(request, 'sign_up.html', {'form': form})    
   
    if request.method == 'POST':
        form = RegisterForm(request.POST) 
        if form.is_valid():
            user = form.save(commit=False)
            user.username = user.username.lower()
            user.save()
            messages.success(request, 'You have singed up successfully.')
            login(request, user)
            return redirect('home')
        else:
            return render(request, 'sign_up.html', {'form': form})



def login_user(request):
    if request.method =='GET':
        form =LoginForm()
        return render(request , 'login.html',{'form':form})
    
    if request.method == 'POST':
        form =LoginForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            user =authenticate(request , username=data['username'] , password=data['password'])
            if user is not None:
                login(request,user)
                messages.success(request, 'login successfully.')
                return redirect('home')
            else:
                messages.error(request,'username or password is wrong' ,'danger')
                



def logout_user(request):
    logout(request)
    messages.success(request,'logout succeessfully','success')
    return redirect('home')
