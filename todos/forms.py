from django import forms
from .models import Work


class WorkForm(forms.ModelForm):
    done =forms.CharField(required=None)
    class Meta:
        model=Work
        fields =('title','body','created_date')



class WorkUpdateForm(forms.ModelForm):
    class Meta:
        model=Work
        fields = '__all__'