from django.db import models


class Work(models.Model):
    title = models.CharField(max_length=200)
    body=models.TextField()
    done =models.TextField(blank=True , null=True)
    created_date = models.DateTimeField()
    

 
    def __str__(self):
        return str(self.title)+"/"+(self.id)
