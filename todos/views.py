from django.shortcuts import render , redirect
from .models import Work
from django.contrib import messages
from .forms import *

def home(request):
    all=Work.objects.all()
    return render(request , 'home.html',{'todos':all})



def detail(request , todo_id ):
   todo= Work.objects.get(id=todo_id)
   return render(request ,'detail.html',{'todo':todo})


def delete(request , todo_id):
    Work.objects.get(id=todo_id).delete()
    messages.success(request , 'todo was deleted' , 'success' )
    return redirect('home')


def createFrom(request):
    if request.method == 'POST':
        form = WorkForm(request.POST)
        if form.is_valid():
            data=form.cleaned_data
            Work.objects.create(title=data['title'],created_date=data['created_date'] ,done=data['done'] , body=data['body'])
            messages.success(request ,'todo was created' ,'success')
            return redirect ('home')
        
    else:
        form = WorkForm()
    return render(request ,'createForm.html' , {'form':form})



def update(request , todo_id):
    todo = Work.objects.get(id=todo_id)
    if request.method=='POST':
        form=WorkUpdateForm(request.POST , instance=todo)
        if form.is_valid():
            form.save()
            messages.success(request ,'updated successfully' ,'success')
            return redirect('detail' ,todo_id)
    else:
        form= WorkUpdateForm(instance=todo)
    return render(request,'update.html',{'form':form})





    